# UULUR
the Unix and Unix Like User Repository(Temporary name) is like the AUR, except for all of Linux, MacOS, and BSD Operating Systems.

## Goals
Ordered from most important to least important in their categories
### Operating System Support
- [ ] Linux support

- [ ] MacOS Support

- [ ] FreeBSD Support

- [ ] OpenBSD Support

### Packaging
- [ ] package "Hello, World!" program to deb file
- [ ] package "Hello, World!" program to rpm file
- [ ] package "Hello, World!" program to dmg file

### Other
- [x] Figure out syntax for UNIXPKG
- [ ] Parse UNIXPKG file
- [ ] Get simple "Hello, World!" compiled from C to binary using UULUR
